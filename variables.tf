variable "name" {
  type        = string
  description = "Name to assign created resources"
}

variable "lambda_bucket_name" {
  type        = string
  description = "Name to assign the S3 bucket that stores Lambda function code"
  default     = null
}

variable "log_retention" {
  type        = string
  description = "Lambda log retention in days"
  default     = 30
}

variable "config" {
  type        = map(any)
  description = "JSON structured mirroring configuration as per https://github.com/aws-samples/aws-vpc-traffic-mirroring-source-automation/blob/master/traffic_mirroring/config/%3CINSERT_REGION_NAME%3E.yaml"
}

variable "virtual_network_id" {
  type        = number
  description = "The unique VXLAN network identifier that is included in the encapsulated mirrored packet that is sent to the target"
  default     = 1169
}

variable "tags" {
  type        = map(string)
  description = "Map of tags to add to resources"
  default     = {}
}
