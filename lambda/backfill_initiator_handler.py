import boto3
import botocore
import logging
import os
from app_helper import publish_message, START_BACKFILL_TOKEN

log = logging.getLogger()
log.setLevel(logging.INFO)

sns = boto3.client('sns')

def lambda_handler(event, context):
    log.info("Initiating setting up traffic mirroring for existing instances")
    sns_topic_arn = os.environ['SNS_TOPIC_ARN']
    publish_message(sns, sns_topic_arn, START_BACKFILL_TOKEN)
