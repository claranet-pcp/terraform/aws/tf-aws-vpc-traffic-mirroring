# tf-aws-vpc-traffic-mirroring

This module sets up various Lambda functions to automate the creation of traffic mirror sources based on instance launch events.

It has been adapted from the solution [here](https://github.com/aws-samples/aws-vpc-traffic-mirroring-source-automation).

Functions are also created to aid in creating sources for existing instances. The initiator function can be invoked via a method of your choice. No payload is required.

## Usage

Example for creating mirror sessions based on an instance tag:

```
module "vpc_traffic_mirroring" {
  source = "../"
  name   = "vpc-traffic-mirroring"

  config = {
    "tags" : [
      {
        "tagList" : [
          {
            "Key" : "mirror-me",
            "Value" : "true"
          }
        ],
        "targetId" : "tmt-00dc0bf3de2ed2737",
        "filterId" : "tmf-0c8041f369ae7bace"
      }
    ]
  }
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
#### Requirements

No requirements.

#### Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

#### Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_backfill"></a> [backfill](#module\_backfill) | git::https://github.com/claranet/terraform-aws-lambda-builder.git | v1.4.2 |
| <a name="module_backfill_initiator"></a> [backfill\_initiator](#module\_backfill\_initiator) | git::https://github.com/claranet/terraform-aws-lambda-builder.git | v1.4.2 |
| <a name="module_cloudwatch_events"></a> [cloudwatch\_events](#module\_cloudwatch\_events) | git::https://github.com/claranet/terraform-aws-lambda-builder.git | v1.4.2 |

#### Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_event_rule.cloudwatch_events](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_rule) | resource |
| [aws_cloudwatch_event_target.cloudwatch_events](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target) | resource |
| [aws_cloudwatch_log_group.backfill](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_cloudwatch_log_group.backfill_initiator](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_cloudwatch_log_group.cloudwatch_events](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_iam_role.lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_lambda_permission.cloudwatch_events](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_lambda_permission.sns](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_s3_bucket.lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_server_side_encryption_configuration.lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_server_side_encryption_configuration) | resource |
| [aws_sns_topic.backfill](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic) | resource |
| [aws_sns_topic_subscription.backfill](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_subscription) | resource |
| [aws_ssm_parameter.config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.lambda_assume](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_partition.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/partition) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

#### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_config"></a> [config](#input\_config) | JSON structured mirroring configuration as per https://github.com/aws-samples/aws-vpc-traffic-mirroring-source-automation/blob/master/traffic_mirroring/config/%3CINSERT_REGION_NAME%3E.yaml | `map(any)` | n/a | yes |
| <a name="input_lambda_bucket_name"></a> [lambda\_bucket\_name](#input\_lambda\_bucket\_name) | Name to assign the S3 bucket that stores Lambda function code | `string` | `null` | no |
| <a name="input_log_retention"></a> [log\_retention](#input\_log\_retention) | Lambda log retention in days | `string` | `30` | no |
| <a name="input_name"></a> [name](#input\_name) | Name to assign created resources | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Map of tags to add to resources | `map(string)` | `{}` | no |
| <a name="input_virtual_network_id"></a> [virtual\_network\_id](#input\_virtual\_network\_id) | The unique VXLAN network identifier that is included in the encapsulated mirrored packet that is sent to the target | `number` | `1169` | no |

#### Outputs

| Name | Description |
|------|-------------|
| <a name="output_backfill_initiator_function_name"></a> [backfill\_initiator\_function\_name](#output\_backfill\_initiator\_function\_name) | Backfill initiator function name. Invoke manually to backfill mirror sources |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
