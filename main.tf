locals {
  tags = merge(
    {
      Name = var.name
    },
    var.tags
  )
}

resource "aws_s3_bucket" "lambda" {
  bucket = try(var.lambda_bucket_name, var.name)

  tags = local.tags
}

resource "aws_s3_bucket_server_side_encryption_configuration" "lambda" {
  bucket = aws_s3_bucket.lambda.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

data "aws_iam_policy_document" "lambda_assume" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "lambda" {
  name               = var.name
  assume_role_policy = data.aws_iam_policy_document.lambda_assume.json

  tags = local.tags
}

data "aws_iam_policy_document" "lambda" {
  statement {
    actions = ["logs:CreateLogGroup"]

    resources = ["arn:${data.aws_partition.current.partition}:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*"]
  }

  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = [
      "arn:${data.aws_partition.current.partition}:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${var.name}-cloudwatch-events:*",
      "arn:${data.aws_partition.current.partition}:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${var.name}-backfill:*",
      "arn:${data.aws_partition.current.partition}:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${var.name}-backfill-initiator:*"
    ]
  }

  statement {
    actions = ["sns:Publish"]

    resources = ["*"]
  }

  statement {
    actions = [
      "ec2:CreateTrafficMirrorSession",
      "ec2:CreateTrafficMirrorTarget",
      "ec2:DescribeInstances",
      "ec2:DescribeTrafficMirrorTargets"
    ]

    resources = ["*"]
  }

  statement {
    actions = [
      "ec2:CreateTags",
      "ec2:DeleteTags",
      "ec2:RunInstances",
      "ec2:TerminateInstances"
    ]

    resources = ["*"]
  }

  statement {
    actions = ["ssm:GetParameter"]

    resources = [aws_ssm_parameter.config.arn]
  }
}

resource "aws_iam_role_policy" "lambda" {
  name   = var.name
  role   = aws_iam_role.lambda.name
  policy = data.aws_iam_policy_document.lambda.json
}

resource "aws_ssm_parameter" "config" {
  name  = "/${var.name}/config"
  type  = "String"
  value = yamlencode(var.config)
}

resource "aws_cloudwatch_log_group" "cloudwatch_events" {
  name              = "/aws/lambda/${var.name}-cloudwatch-events"
  retention_in_days = var.log_retention

  tags = local.tags
}

module "cloudwatch_events" {
  source = "git::https://github.com/claranet/terraform-aws-lambda-builder.git?ref=v1.4.2"

  function_name           = "${var.name}-cloudwatch-events"
  description             = "Creates VPC traffic mirroring sources based on CloudWatch Events."
  handler                 = "cloudwatch_event_handler.lambda_handler"
  runtime                 = "python3.8"
  s3_bucket               = aws_s3_bucket.lambda.id
  create_role             = false
  enable_input_validation = false
  archive_method          = "NATIVE"
  role                    = aws_iam_role.lambda.arn
  timeout                 = 120

  build_mode = "LAMBDA"
  source_dir = "${path.module}/lambda"

  environment = {
    variables = {
      CONFIG_SSM_PARAMETER_NAME = aws_ssm_parameter.config.name
      VIRTUAL_NETWORK_ID        = var.virtual_network_id
    }
  }

  tags = local.tags
}

resource "aws_lambda_permission" "cloudwatch_events" {
  statement_id  = "${var.name}-cloudwatch-events"
  action        = "lambda:InvokeFunction"
  function_name = module.cloudwatch_events.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.cloudwatch_events.arn
}

resource "aws_cloudwatch_event_rule" "cloudwatch_events" {
  name        = var.name
  description = "Instance launch EventRule"

  event_pattern = <<EOF
{
  "source": ["aws.ec2"],
  "detail-type": ["EC2 Instance State-change Notification"],
  "detail": {
    "state": ["running"]
  }
}
EOF
}

resource "aws_cloudwatch_event_target" "cloudwatch_events" {
  rule      = aws_cloudwatch_event_rule.cloudwatch_events.name
  target_id = var.name
  arn       = module.cloudwatch_events.arn
}

resource "aws_sns_topic" "backfill" {
  name = "${var.name}-backfill"
}

resource "aws_sns_topic_subscription" "backfill" {
  topic_arn = aws_sns_topic.backfill.arn
  protocol  = "lambda"
  endpoint  = module.backfill.arn
}

resource "aws_cloudwatch_log_group" "backfill" {
  name              = "/aws/lambda/${var.name}-backfill"
  retention_in_days = var.log_retention

  tags = local.tags
}

module "backfill" {
  source = "git::https://github.com/claranet/terraform-aws-lambda-builder.git?ref=v1.4.2"

  function_name           = "${var.name}-backfill"
  description             = "Creates VPC traffic mirroring sources for existing resources."
  handler                 = "backfill_handler.lambda_handler"
  runtime                 = "python3.8"
  s3_bucket               = aws_s3_bucket.lambda.id
  create_role             = false
  enable_input_validation = false
  archive_method          = "NATIVE"
  role                    = aws_iam_role.lambda.arn
  timeout                 = 120

  build_mode = "LAMBDA"
  source_dir = "${path.module}/lambda"

  environment = {
    variables = {
      CONFIG_SSM_PARAMETER_NAME = aws_ssm_parameter.config.name
      VIRTUAL_NETWORK_ID        = var.virtual_network_id
    }
  }

  tags = local.tags
}

resource "aws_lambda_permission" "sns" {
  statement_id  = "${var.name}-backfill"
  action        = "lambda:InvokeFunction"
  function_name = module.backfill.function_name
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.backfill.arn
}

resource "aws_cloudwatch_log_group" "backfill_initiator" {
  name              = "/aws/lambda/${var.name}-backfill-initiator"
  retention_in_days = var.log_retention

  tags = local.tags
}

module "backfill_initiator" {
  source = "git::https://github.com/claranet/terraform-aws-lambda-builder.git?ref=v1.4.2"

  function_name           = "${var.name}-backfill-initiator"
  description             = "Initiates backfill function (manual invocation)."
  handler                 = "backfill_initiator_handler.lambda_handler"
  runtime                 = "python3.8"
  s3_bucket               = aws_s3_bucket.lambda.id
  create_role             = false
  enable_input_validation = false
  archive_method          = "NATIVE"
  role                    = aws_iam_role.lambda.arn
  timeout                 = 120

  build_mode = "LAMBDA"
  source_dir = "${path.module}/lambda"

  environment = {
    variables = {
      CONFIG_SSM_PARAMETER_NAME = aws_ssm_parameter.config.name
      SNS_TOPIC_ARN             = aws_sns_topic.backfill.arn
      VIRTUAL_NETWORK_ID        = var.virtual_network_id
    }
  }

  tags = local.tags
}
