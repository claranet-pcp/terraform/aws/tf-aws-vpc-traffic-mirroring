output "backfill_initiator_function_name" {
  description = "Backfill initiator function name. Invoke manually to backfill mirror sources"
  value       = module.backfill_initiator.function_name
}
